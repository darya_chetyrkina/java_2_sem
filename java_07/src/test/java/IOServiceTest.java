import org.junit.Test;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class IOServiceTest {

    @Test
    public void writeIntArrayToByteStreamTest() throws IOException {

        int[] intArray = {1,2,-3,256};
        byte[] byteArray = {0,0,0,1,  0,0,0,2,  -1,-1,-1,-3,  0,0,1,0};
        ByteArrayOutputStream actualStream = new ByteArrayOutputStream(byteArray.length);
        IOService.writeIntArrayToByteStream(intArray,actualStream);
        assertArrayEquals(byteArray, actualStream.toByteArray());
        actualStream.close();
    }

    @Test
    public void readIntArrayFromByteStreamTest() throws IOException{
        int[] expected = {1,2,-3,256};
        byte[] byteArray = {0,0,0,1,  0,0,0,2,  -1,-1,-1,-3,  0,0,1,0};
        ByteArrayInputStream in = new ByteArrayInputStream(byteArray);
        int size = 4;
        int[] actual = new int[size];
        IOService.readIntArrayFromByteStream(actual, in);
        assertArrayEquals(expected, actual);
        in.close();
    }

    @Test

    public void writeIntArrayToCharStreamTest() throws Exception{

        int[] intArray = {1,2,-3,256};
        Writer actualStream = new StringWriter();
        IOService.writeIntArrayToCharStream(intArray,actualStream);
        assertEquals("1 2 -3 256 ", actualStream.toString());
        actualStream.close();

    }



    @Test
    public void readIntArrayFromCharStreamTest() throws Exception {

        int[] expected = {1,2,-3,256};
        Reader stream = new StringReader("1 2 -3 256");
        int[] actual = new int[4];
        IOService.readIntArrayFromCharStream(actual, stream);
        assertArrayEquals(expected, actual);
        stream.close();

    }

    @Test
    public void readIntArrayStartWithTest() throws IOException{

        File file = new File("test.txt");
        RandomAccessFile r = new RandomAccessFile(file, "rw");
        r.writeInt(12);
        r.writeInt(3);
        r.writeInt(-1);
        r.writeInt(0);
        r.writeInt(3242);
        r.close();
        int[] expected = {3, -1, 0, 3242};
        int[] actual = new int[4];
        IOService.readIntArrayStartWith(file,actual,4);
        assertArrayEquals(expected, actual);

    }

    @Test
    public void getFilteredFilesInDirectoryTest(){
        File dir = new File(".");
        String filter = ".xml";
        List<String> actual = IOService.getFilteredFilesInDirectory(dir, filter);
        List<String> expected = new ArrayList<>(1);
        expected.add("pom.xml");
        assertEquals(expected, actual);
    }

    @Test
    public void getFilteredFilesAndSubdirectory(){
        File dir = new File("C:\\Users\\Darya\\Documents\\Java\\java_2_sem\\java_07");
        String regEx = "test.*(a-z)*|.gitignore";
        String file1 = "C:\\Users\\Darya\\Documents\\Java\\java_2_sem\\java_07\\test.txt";
        String file2 = "C:\\Users\\Darya\\Documents\\Java\\java_2_sem\\java_07\\.idea\\.gitignore";
        String file3 = "C:\\Users\\Darya\\Documents\\Java\\java_2_sem\\java_07\\src\\test";
        String file4 = "C:\\Users\\Darya\\Documents\\Java\\java_2_sem\\java_07\\target\\generated-test-sources\\test-annotations";
        String file5 = "C:\\Users\\Darya\\Documents\\Java\\java_2_sem\\java_07\\target\\test-classes";
        List<String> result = IOService.getFilteredFilesAndSubdirectory(dir,regEx);
        List<String> expected = new ArrayList<>();
        expected.add(file2);
        expected.add(file3);
        expected.add(file4);
        expected.add("C:\\Users\\Darya\\Documents\\Java\\java_2_sem\\java_07\\target\\maven-status\\maven-compiler-plugin\\testCompile");
        expected.add(file5);
        expected.add(file1);
        assertEquals(expected, result);
    }


}