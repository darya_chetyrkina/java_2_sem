import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class HouseSerializeTest {

    @Test
    public void serializeAndDeserializeHouse() throws IOException, ClassNotFoundException {

        Person person1 = new Person("name", "second name", "famile name", new Date(12, Calendar.JANUARY,12));
        List<Person> personList1 = new ArrayList<>(1);
        personList1.add(person1);
        Flat flat1 = new Flat(1, 70, personList1);
        List<Flat> flatList1 = new ArrayList<>(1);
        flatList1.add(flat1);
        House house = new House("123456", "Omsk", person1, flatList1);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        HouseService.serialize(out, house);
        out.close();

        InputStream in = new ByteArrayInputStream(out.toByteArray());
        House dHouse = HouseService.deserialize(in);
        in.close();

        assertTrue(house.equals(dHouse));

    }

    @Test
    public void serializeAndDeserializeHouseAsString() throws IOException, ClassNotFoundException {

        Person person1 = new Person("name", "second name", "famile name", new Date(12, Calendar.JANUARY,12));
        List<Person> personList1 = new ArrayList<>(1);
        personList1.add(person1);
        Flat flat1 = new Flat(1, 70, personList1);
        List<Flat> flatList1 = new ArrayList<>(1);
        flatList1.add(flat1);
        House house = new House("123456", "Omsk", person1, flatList1);

        String serialized = HouseService.serializeAsString(house);
        System.out.println(serialized);
        House dHouse = HouseService.deserializeAsString(serialized);

        assertTrue(house.equals(dHouse));

    }


    @Test
    public void saveCSV() throws IOException {


        //flat1
        Person person1 = new Person("���������", "����������", "������", new Date(12, Calendar.JANUARY, 12));
        List<Person> personListFlat1 = new ArrayList<>(1);
        personListFlat1.add(person1);
        Flat flat1 = new Flat(1, 40, personListFlat1);
        //flat2
        Person person2 = new Person("�����", "����������", "��������", new Date(12, Calendar.JANUARY, 12));
        List<Person> personListFlat2 = new ArrayList<>(1);
        personListFlat2.add(person2);
        Flat flat2 = new Flat(2, 65, personListFlat2);
        //flat3
        Person person3 = new Person("����", "��������", "��������", new Date(12, Calendar.JANUARY, 12));
        Person person4 = new Person("������", "���������", "�������", new Date(12, Calendar.JANUARY, 12));
        List<Person> personListFlat3 = new ArrayList<>(2);
        personListFlat3.add(person3);
        personListFlat3.add(person4);

        //house
        Flat flat3 = new Flat(3, 58, personListFlat3);
        List<Flat> flatList1 = new ArrayList<>(3);
        flatList1.add(flat1);
        flatList1.add(flat2);
        flatList1.add(flat3);
        Person headOfHouse = new Person("�����", "��������", "������", new Date(12, Calendar.JANUARY, 12));
        House house = new House("12345", "�. ����, ��. ����, 321", headOfHouse, flatList1);
        HouseService.saveCSV(".", house);

        List<String> expected = new ArrayList<>();
        expected.add("������ � ����;");
        expected.add("����������� �����:; 12345;");
        expected.add("�����:; �. ����, ��. ����, 321;");
        expected.add("������� �� ����:; ������ ����� ��������;");
        expected.add("������ � ���������;");
        expected.add("�;�������, ��.�;���������;");
        expected.add("1;40,000000.;������ �.�.;");
        expected.add("2;65,000000.;�������� �.�.;");
        expected.add("3;58,000000.;�������� �.�.,������� �.�.;");

        File file = new File("house_12345.csv");
        List<String> actual = Files.readAllLines(file.toPath(), Charset.forName("windows-1251"));
        assertEquals(expected, actual);

    }
}