import org.junit.Test;

import java.sql.Date;

import static org.junit.Assert.*;

public class PersonTest {

    @Test
    public void getFirstName() {
        Person person1 = new Person("Ivan","Ivanovich","Ivanov", new Date(2000,01,13));
        assertEquals("Ivan", person1.getFirstName());
    }

    @Test
    public void getSecondName() {
        Person person1 = new Person("Ivan","Ivanovich","Ivanov", new Date(2000,01,13));
        assertEquals("Ivanovich", person1.getSecondName());
    }

    @Test
    public void getFamilyName() {
        Person person1 = new Person("Ivan","Ivanovich","Ivanov", new Date(2000,01,13));
        assertEquals("Ivanov", person1.getFamilyName());

    }

    @Test
    public void getBirthDay() {
        Person person1 = new Person("Ivan","Ivanovich","Ivanov", new Date(2000,01,13));
        assertEquals(new Date(2000,01,13), person1.getBirthDay());
    }

    @Test
    public void testEquals() {
        Person person1 = new Person("Ivan","Ivanovich","Ivanov", new Date(2000,01,13));
        Person person2 = new Person("Ilya","Ivanovich","Ivanov", new Date(1998,02,23));
        assertFalse(person1.equals(person2));
    }

    @Test
    public void testHashCode() {
        Person person1 = new Person("Ivan","Ivanovich","Ivanov", new Date(2000,01,13));
        Person person2 = new Person("Ilya","Ivanovich","Ivanov", new Date(1998,02,23));
        assertNotEquals(person1.hashCode(), person2.hashCode());
    }
}