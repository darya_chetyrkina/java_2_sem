import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Flat implements Serializable {

    private int number;
    private int square;
    private List<Person> tenants;

    public Flat(){}
    public Flat(int number, int square, List<Person> tenants) {
        this.setNumber(number);
        this.setSquare(square);
        this.setTenants(tenants);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        if (number < 0) throw new IllegalArgumentException("");
        this.number = number;
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(int square) {
        if (square < 0) throw new IllegalArgumentException("");
        this.square = square;
    }

    public List<Person> getTenants() {
        return tenants;
    }

    public void setTenants(List<Person> tenants) {
        if (tenants == null) throw new IllegalArgumentException("");
        this.tenants = tenants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number && Double.compare(flat.square, square) == 0 && tenants.equals(flat.tenants);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, square, tenants);
    }
}
