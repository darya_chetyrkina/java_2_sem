import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Person implements Serializable {

    private String firstName;
    private String secondName;
    private String familyName;

    private Date birthDay;

    public Person(){}

    public Person(String firstName, String secondName, String familyName, Date birthDay) {
        this.setBirthDay(birthDay);
        this.setFamilyName(familyName);
        this.setSecondName(secondName);
        this.setFirstName(firstName);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setFirstName(String firstName) {
        if (firstName == null) throw new IllegalArgumentException("");
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        if (secondName == null) throw new IllegalArgumentException("");
        this.secondName = secondName;
    }

    public void setFamilyName(String familyName) {
        if (familyName == null) throw new IllegalArgumentException("");
        this.familyName = familyName;
    }

    public void setBirthDay(Date birthDay) {
        if (birthDay == null) throw new IllegalArgumentException("");
        this.birthDay = birthDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) && Objects.equals(secondName, person.secondName) && Objects.equals(familyName, person.familyName) && Objects.equals(birthDay, person.birthDay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, familyName, birthDay);
    }


}
