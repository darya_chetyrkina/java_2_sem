import java.io.*;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {

    private String cadastralNumber;
    private String address;
    private Person headOfHouse;
    private List<Flat> flats;

    public House(){}
    public House(String cadastralNumber, String address, Person headOfHouse, List<Flat> flats) {
        this.setCadastralNumber(cadastralNumber);
        this.setAddress(address);
        this.setHeadOfHouse(headOfHouse);
        this.setFlats(flats);
    }

    public String getCadastralNumber() {
        return cadastralNumber;
    }

    public void setCadastralNumber(String cadastralNumder) {
        if (cadastralNumder == null) throw new IllegalArgumentException("cadastralNumder is null");
        this.cadastralNumber = cadastralNumder;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        if (address == null) throw new IllegalArgumentException("");
        this.address = address;
    }

    public Person getHeadOfHouse() {
        return headOfHouse;
    }

    public void setHeadOfHouse(Person headOfHouse) {
        if (headOfHouse == null) throw new IllegalArgumentException("");
        this.headOfHouse = headOfHouse;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public void setFlats(List<Flat> flats) {
        if (flats == null) throw new IllegalArgumentException("");
        this.flats = flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return cadastralNumber.equals(house.cadastralNumber) && address.equals(house.address)
                && headOfHouse.equals(house.headOfHouse) && flats.equals(house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cadastralNumber, address, headOfHouse, flats);
    }


}
