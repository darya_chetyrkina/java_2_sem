import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class HouseService {

    public static void serialize(OutputStream out, House house) throws IOException{

        if (house == null) throw new IllegalArgumentException("House is null");

        try(ObjectOutputStream obj = new ObjectOutputStream(out)){
            obj.writeObject(house);
        }
    }

    public static House deserialize(InputStream in) throws IOException, ClassNotFoundException {


        try (ObjectInputStream obj = new ObjectInputStream(in)){
            return (House) obj.readObject();
        }
    }

    public static String serializeAsString(House house) throws JsonProcessingException {

        if (house == null) throw new IllegalArgumentException("House is null");

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(house);
    }

    public static House deserializeAsString(String jsonString) throws JsonProcessingException{

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonString, House.class);
    }

    public static void saveCSV(String pathname, House house) throws IOException {

        File saveFile = new File(String.format("%s\\house_%s.csv", pathname, house.getCadastralNumber()));

        try (PrintWriter writer = new PrintWriter(new FileWriter(saveFile))) {
            writer.println("������ � ����;");
            writer.println(String.format("����������� �����:; %s;", house.getCadastralNumber()));
            writer.println(String.format("�����:; %s;", house.getAddress()));
            writer.println(String.format("������� �� ����:; %s %s %s;", house.getHeadOfHouse().getFamilyName(),
                    house.getHeadOfHouse().getFirstName(), house.getHeadOfHouse().getSecondName()));
            writer.println("������ � ���������;");
            writer.println("�;�������, ��.�;���������;");
            for (Flat flat : house.getFlats()) {

                List<String> abbreviatedName = new ArrayList<>(flat.getTenants().size());
                for (Person person : flat.getTenants()) {
                    abbreviatedName.add(String.format("%s %s.%s.", person.getFamilyName(), person.getFirstName().charAt(0),
                            person.getSecondName().charAt(0)));
                }
                writer.println(String.format("%d;%f.;%s;", flat.getNumber(), flat.getSquare(), String.join(",",abbreviatedName)));

            }
        }
    }
}
