import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class IOService {

    public static void writeIntArrayToByteStream(int[] array, OutputStream out) throws IOException{

        try (DataOutputStream outStream = new DataOutputStream(out)){

            for (int i : array) {
                outStream.writeInt(i);
            }

        }

    }

    public static void readIntArrayFromByteStream(int[] array, InputStream in) throws IOException{

        try (DataInputStream inStream = new DataInputStream(in)){

            for (int i = 0; i < array.length; i++) {
                array[i] = inStream.readInt();
            }

        }
    }

    public static void writeIntArrayToCharStream(int[] array, Writer writer)  {

        try(PrintWriter wr = new PrintWriter(writer)){
            for(int i: array) {
                wr.print(i);
                wr.write(" ");
            }
        }


    }

    public static void readIntArrayFromCharStream(int[] array, Reader reader) {

        Scanner scanner = new Scanner(reader);
        for (int i = 0; i < array.length; i++){
            if(scanner.hasNextInt()) array[i] = scanner.nextInt();
        }

    }

    public static void readIntArrayStartWith(File file, int[] array, long position) throws IOException {

        try (RandomAccessFile rfile = new RandomAccessFile(file, "r")) {
            rfile.seek(position);
            for (int i = 0; i < array.length; i++) {
                array[i] = rfile.readInt();
            }
        }

    }

    public static List<String> getFilteredFilesInDirectory(File dir, String filter){

        if (!dir.isDirectory() || !dir.exists()) throw new IllegalArgumentException("Directory expected");
        String[] array = dir.list(new ExtensionFilter(filter));
        return Arrays.asList(array);
    }


    public static List<String> getFilteredFilesAndSubdirectory(File dir, String regEx) {
/*
    *��������� ����� File, �������� � �������� �������� ������ ���� ������ � ������������,
    ����� ������� ������������� ��������� ����������� ���������. ����� ������
    ���������������� � �����������. ����� ��������� ������ ������ ���� ������ �
    ����������� ������.

*/
        if (!dir.isDirectory() || !dir.exists()) throw new IllegalArgumentException("Directory expected");

        List<String> result = new ArrayList<>();
        File[] list = dir.listFiles();

        if (list == null) return result;

        Pattern pattern = Pattern.compile(regEx);

        for (File f:list) {
            if (pattern.matcher(f.getName()).matches()) result.add(f.getAbsolutePath());
            if(f.isDirectory()){result.addAll(getFilteredFilesAndSubdirectory(f,regEx));}
        }

        return result;

    }
}
