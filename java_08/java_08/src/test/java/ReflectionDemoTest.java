import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ReflectionDemoTest {

    public static class Teacher extends Human{}
    public static class Relative extends Human{}
    public static class Father extends Relative{}
    public static class DistantRelative extends Relative{}
    public static class SecondCousins extends DistantRelative{}

    public static class Class1{
        public void Method1(){}
    }
    public static class Class2 extends Class1{
        private int Method2(){return 0;}

        @Override
        public String toString() {
            return "Class2{}";
        }
    }


    @Test
    public void toCountHumans() {

        Human human1 = new DistantRelative();
        Object object = new Object();
        Human human2 = new Teacher();
        Teacher teacher = new Teacher();
        Human human3 = new Father();
        List<Object> list = new ArrayList<>();
        list.add(human1);
        list.add(human2);
        list.add(human3);
        list.add(object);
        list.add(teacher);
        assertEquals(4,ReflectionDemo.toCountHumans(list));
    }

    @Test
    public void toCountHumans2() {

        Object object = new Object();
        List<Object> list = new ArrayList<>();
        list.add(object);
        assertEquals(0,ReflectionDemo.toCountHumans(list));
    }

    @Test
    public void getListOfPublicMethods() {

        Class2 class2 = new Class2();
        List<String> expected = new ArrayList<>();
        expected.add("Method1");
        expected.add("wait");
        expected.add("wait");
        expected.add("wait");
        expected.add("equals");
        expected.add("toString");
        expected.add("hashCode");
        expected.add("getClass");
        expected.add("notify");
        expected.add("notifyAll");
        Collections.sort(expected);
        List<String> actual = ReflectionDemo.getListOfPublicMethods(class2);
        Collections.sort(actual);
        assertEquals(expected, actual);
    }

    @Test
    public void getListOfSuperClasses() {

        SecondCousins cousins = new SecondCousins();
        List<String> expected = new ArrayList<>();
        expected.add("ReflectionDemoTest$DistantRelative");
        expected.add("ReflectionDemoTest$Relative");
        expected.add("Human");
        expected.add("java.lang.Object");
        assertEquals(expected, ReflectionDemo.getListOfSuperClasses(cousins));
    }

    @Test
    public void getListOfSuperClasses2() {

        Object obj = new Object();
        List<String> expected = new ArrayList<>();
        assertEquals(expected, ReflectionDemo.getListOfSuperClasses(obj));
    }

}