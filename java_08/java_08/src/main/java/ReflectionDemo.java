import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo{

    public static int toCountHumans(List<?> list){

        int count = 0;

        for(Object obj: list){

            if(obj instanceof Human) count++;
        }

        return count;
    }

    public static List<String> getListOfPublicMethods(Object obj){

        List<String> result = new ArrayList<>();

        for(Method method: obj.getClass().getMethods()){
            result.add(method.getName());
        }

        return result;

    }

    public static List<String> getListOfSuperClasses(Object obj){

        List<String> result = new ArrayList<>();

        Class<?> current = obj.getClass();

        while(current.getSuperclass() != null){

            current = current.getSuperclass();
            result.add(current.getName());

        };

        return result;
    }
}