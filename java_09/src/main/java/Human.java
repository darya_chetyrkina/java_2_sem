import java.util.Objects;

public class Human {

    private String firstName;
    private String secondName;
    private String familyName;
    private int age;
    public enum GENDER{MALE, FEMALE}
    private GENDER gender;

    public Human() {
        this.firstName = "";
        this.secondName = "";
        this.familyName = "";
        this.age = 0;
    }

    public Human(String firstName, String secondName, String familyName, int age, GENDER gender) {

        if (firstName == null || secondName == null || familyName == null || age < 0)
            throw new IllegalArgumentException("Param don't correct.");
        this.firstName = firstName;
        this.secondName = secondName;
        this.familyName = familyName;
        this.age = age;
        this.gender = gender;
    }

    public Human(Human human) {
        this.firstName = new String(human.getFirstName());
        this.secondName = new String(human.getSecondName());
        this.familyName = new String(human.getFamilyName());
        this.age = human.getAge();
        this.gender = human.gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName != null) this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        if (secondName != null) this.secondName = secondName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        if (familyName != null) this.familyName = familyName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(GENDER gender){ this.gender = gender;}

    public GENDER getGender(){return this.gender;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getAge() == human.getAge() && getFirstName().equals(human.getFirstName()) && getSecondName().equals(human.getSecondName()) && getFamilyName().equals(human.getFamilyName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getSecondName(), getFamilyName(), getAge());
    }

}