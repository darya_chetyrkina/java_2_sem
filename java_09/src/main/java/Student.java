import java.util.Objects;

public class Student extends Human{

    private String university;
    private String faculty;
    private String specialty;

    public Student(String firstName, String secondName, String familyName, int age, GENDER gender, String university, String faculty, String specialty){

        super(firstName,secondName,familyName,age,gender);

        if (university == null || faculty == null ||  specialty == null) throw new IllegalArgumentException("Argument was null");

        setUniversity(university);
        setFaculty(faculty);
        setSpecialty(specialty);
    }

    public Student(Human human, String university, String faculty, String specialty){

        super(human);

        if (university == null || faculty == null ||  specialty == null) throw new IllegalArgumentException("Argument was null");

        setUniversity(university);
        setFaculty(faculty);
        setSpecialty(specialty);
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        if (university == null) throw new IllegalArgumentException("Argument was null");
        this.university = university;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        if (faculty == null) throw new IllegalArgumentException("Argument was null");
        this.faculty = faculty;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        if (specialty == null) throw new IllegalArgumentException("Argument was null");
        this.specialty = specialty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return getUniversity().equals(student.getUniversity()) && getFaculty().equals(student.getFaculty()) && getSpecialty().equals(student.getSpecialty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getUniversity(), getFaculty(), getSpecialty());
    }

}
