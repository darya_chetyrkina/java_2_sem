import java.util.function.*;

public class LambdaRunner {

    public static Object functionOfString(Function<String, ?> func, String string){
        return func.apply(string);
    }

    public static Object functionOfHuman(Function<Human, ?> func, Human human){
        return func.apply(human);
    }

    public static boolean biPredicate(BiPredicate<Human,Human> func, Human human1, Human human2){
        return func.test(human1,human2);
    }

    public static boolean functionOfThreeHuman(IToBooleanFunction<Human,Human,Human> func, Human human, Human human2, Human human3, int maxAge){
        return func.apply(human,human2,human3,maxAge);
    }

}
