
@FunctionalInterface
public interface IToBooleanFunction<T,R,U> {
    boolean apply(Human human1, Human human2, Human human3, Integer maxAge);
}
