import java.util.Arrays;
import java.util.Optional;
import java.util.function.*;

public class LambdaDemo {

    public static final Function<String,Integer> countChar = String::length;

    public static final Function<String, Character> getFirstChar = str->Optional.ofNullable(str)
                                                                        .map(s->s.charAt(0))
                                                                        .orElse(null);

    public static final Function<String,Boolean> isNotItStringContainsSpace = str->!str.contains(" ");

    public static final Function<String,Integer> countWords = str-> str.isEmpty()?0
                                                                    :str.split("[\\p{Graph}\\s]?,+[\\p{Graph}\\s]?").length;



    public static final Function<Human,Integer> getAge = Human::getAge;

    public static final BiPredicate<Human, Human> isFamilyNamesEqual = (human1, human2)->human1.getFamilyName()
                                                                        .equals(human2.getFamilyName());

    public static final Function<Human,String> fullName = human->String.format("%s %s %s"
                                                                ,human.getFirstName()
                                                                ,human.getSecondName()
                                                                ,human.getFamilyName());

    public static final Function<Human,Human> createHumanAgedUpOneYear = human->new Human(human.getFirstName()
                                                                            ,human.getSecondName()
                                                                            ,human.getFamilyName()
                                                                            ,human.getAge()+1
                                                                            ,human.getGender());

    public static final IToBooleanFunction<Human,Human,Human> isAllHumanAgesUnderThan = (human1, human2, human3, maxAge)->
                                                                human1.getAge()<maxAge
                                                                        && human2.getAge()<maxAge
                                                                        && human3.getAge()<maxAge;
}
