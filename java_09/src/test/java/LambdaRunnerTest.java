import org.junit.Test;

import static org.junit.Assert.*;

public class LambdaRunnerTest {

    @Test
    public void countChar(){
        assertEquals(5, LambdaRunner.functionOfString(LambdaDemo.countChar,"12345"));
    }
    @Test
    public void getFirstChar1(){
        char actual = LambdaDemo.getFirstChar.apply("12345");
        assertEquals('1', LambdaRunner.functionOfString(LambdaDemo.getFirstChar, "12345"));
    }
    @Test
    public void getFirstChar2(){
        assertNull(LambdaRunner.functionOfString(LambdaDemo.getFirstChar, null));
    }
    @Test
    public void isNotItContainsStringSpace1(){
        assertFalse((Boolean) LambdaRunner.functionOfString(LambdaDemo.isNotItStringContainsSpace,"It contains white space"));
    }
    @Test
    public void isNotItContainsStringSpace2(){
        assertTrue((Boolean) LambdaRunner.functionOfString(LambdaDemo.isNotItStringContainsSpace,"Itdoesnotcontainwhitespace"));
    }
    @Test
    public void countWords(){
        assertEquals(5,LambdaRunner.functionOfString(LambdaDemo.countWords,"This,message,contains,five,words"));
    }
    @Test
    public void countWords2(){
        assertEquals(7,LambdaRunner.functionOfString(LambdaDemo.countWords,",,,,,,This,сообщение ,, ,,,contains,five,words,,,,\t"));
    }

    @Test
    public void getAge1(){
        Human student = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        assertEquals(24, LambdaRunner.functionOfHuman(LambdaDemo.getAge,student));
    }
    @Test
    public void getAge2(){
        Student student = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        assertEquals(24, LambdaRunner.functionOfHuman(LambdaDemo.getAge,student));
    }
    @Test
    public void isFamilyNamesEqual1(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human human2 = new Human("Name2","Second Name","Family Name",24, Human.GENDER.MALE);
        assertTrue(LambdaRunner.biPredicate(LambdaDemo.isFamilyNamesEqual,human1,human2));
    }
    @Test
    public void isFamilyNamesEqual2(){
        Student student1 = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        Student student2 = new Student("Name2","Second Name2","Family Name",24, Human.GENDER.MALE, "OMSU", "Math","Applied Math");
        assertTrue(LambdaRunner.biPredicate(LambdaDemo.isFamilyNamesEqual,student1,student2));
    }
    @Test
    public void isFamilyNamesEqual3(){
        Student student1 = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        Student student2 = new Student("Name2","Second Name2","Family Name2",24, Human.GENDER.MALE, "OMSU", "Math","Applied Math");
        assertFalse(LambdaRunner.biPredicate(LambdaDemo.isFamilyNamesEqual,student1,student2));
    }
    @Test
    public void fullName(){
        Student student1 = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        assertEquals("Name Second Name Family Name", LambdaRunner.functionOfHuman(LambdaDemo.fullName,student1));
    }
    @Test
    public void createHumanAndAgeUpOneYear(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human expected = new Human("Name","Second Name","Family Name",25, Human.GENDER.FEMALE);
        assertEquals(expected,LambdaRunner.functionOfHuman(LambdaDemo.createHumanAgedUpOneYear,human1));
    }
    @Test
    public void isAllHumanUnderThanMaxAge1(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human human2 = new Human("Name","Second Name","Family Name",25, Human.GENDER.FEMALE);
        Human human3 = new Human("Name","Second Name","Family Name",26, Human.GENDER.FEMALE);
        assertTrue(LambdaRunner.functionOfThreeHuman(LambdaDemo.isAllHumanAgesUnderThan,human1,human2,human3,30));

    }
    @Test
    public void isAllHumanUnderThanMaxAge2(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human human2 = new Human("Name","Second Name","Family Name",25, Human.GENDER.FEMALE);
        Human human3 = new Human("Name","Second Name","Family Name",26, Human.GENDER.FEMALE);
        assertFalse(LambdaRunner.functionOfThreeHuman(LambdaDemo.isAllHumanAgesUnderThan,human1,human2,human3,26));

    }
}