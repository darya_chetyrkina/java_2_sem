import org.junit.Test;
import static org.junit.Assert.*;

public class LambdaDemoTest {

    @Test
    public void countChar(){
        assertEquals(5, (int)LambdaDemo.countChar.apply("12345"));
    }
    @Test
    public void getFirstChar1(){
        char expected = '1';
        char actual = LambdaDemo.getFirstChar.apply("12345");
        assertEquals(expected, actual);
    }
    @Test
    public void getFirstChar2(){
        assertNull(LambdaDemo.getFirstChar.apply(null));
    }
    @Test
    public void isNotItContainsStringSpace1(){
        assertFalse(LambdaDemo.isNotItStringContainsSpace.apply("It contains white space"));
    }
    @Test
    public void isNotItContainsStringSpace2(){
        assertTrue(LambdaDemo.isNotItStringContainsSpace.apply("Itdoesnotcontainwhitespace"));
    }
    @Test
    public void countWords(){
        assertEquals(7,(int)LambdaDemo.countWords.apply(" ,,,,,,This,сообщение ,, ,,,contains,five,words,,,,\t"));
    }
    @Test
    public void countWords2(){
        assertEquals(1,(int)LambdaDemo.countWords.apply(" "));
    }
    @Test
    public void countWords3(){
        assertEquals(0,(int)LambdaDemo.countWords.apply(""));
    }
    @Test
    public void getAge1(){
        Human student = new Human("Name","Second Name","FamilyName",24, Human.GENDER.FEMALE);
        assertEquals(24, (int)LambdaDemo.getAge.apply(student));
    }
    @Test
    public void getAge2(){
        Student student = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        assertEquals(24, (int)LambdaDemo.getAge.apply(student));
    }
    @Test
    public void isFamilyNamesEqual1(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human human2 = new Human("Name2","Second Name","Family Name",24, Human.GENDER.MALE);
        assertTrue(LambdaDemo.isFamilyNamesEqual.test(human1,human2));
    }
    @Test
    public void isFamilyNamesEqual2(){
        Student student1 = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        Student student2 = new Student("Name2","Second Name2","Family Name",24, Human.GENDER.MALE, "OMSU", "Math","Applied Math");
        assertTrue(LambdaDemo.isFamilyNamesEqual.test(student1,student2));
    }
    @Test
    public void isFamilyNamesEqual3(){
        Student student1 = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        Student student2 = new Student("Name2","Second Name2","Family Name2",24, Human.GENDER.MALE, "OMSU", "Math","Applied Math");
        assertFalse(LambdaDemo.isFamilyNamesEqual.test(student1,student2));
    }
    @Test
    public void fullName(){
        Student student1 = new Student("Name","Second Name","Family Name",24, Human.GENDER.FEMALE, "OMSU", "Math","Applied Math");
        assertEquals("Name Second Name Family Name", LambdaDemo.fullName.apply(student1));
    }
    @Test
    public void createHumanAndAgeUpOneYear(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human expected = new Human("Name","Second Name","Family Name",25, Human.GENDER.FEMALE);
        assertEquals(expected,LambdaDemo.createHumanAgedUpOneYear.apply(human1));
    }
    @Test
    public void isAllHumanUnderThanMaxAge1(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human human2 = new Human("Name","Second Name","Family Name",25, Human.GENDER.FEMALE);
        Human human3 = new Human("Name","Second Name","Family Name",26, Human.GENDER.FEMALE);
        assertTrue(LambdaDemo.isAllHumanAgesUnderThan.apply(human1,human2,human3,30));

    }
    @Test
    public void isAllHumanUnderThanMaxAge2(){
        Human human1 = new Human("Name","Second Name","Family Name",24, Human.GENDER.FEMALE);
        Human human2 = new Human("Name","Second Name","Family Name",25, Human.GENDER.FEMALE);
        Human human3 = new Human("Name","Second Name","Family Name",26, Human.GENDER.FEMALE);
        assertFalse(LambdaDemo.isAllHumanAgesUnderThan.apply(human1,human2,human3,26));

    }

}