import java.util.Comparator;

public class HumanComparatorDescending implements Comparator<Human> {

    @Override
    public int compare(Human o1, Human o2) {
        return -1*o1.compareTo(o2);
    }
}