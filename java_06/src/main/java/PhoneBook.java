import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhoneBook {
/*    �������� ����� PhoneBook (���������� �����). ����� ������������ ����� �����������
    �������� � ������ ��� ������� ��������� (����� �������� � ������). ������: ��������
    �������, ������� �������, �������� ������ ��������� �� ��������, ����� �������� ��
    ������ ��������, ����� ���� ����� � �� ���������� �� ������ ������� ��������
            (��������� � ����� ����������� ����� �� ���������, �� ���������� ������ ����������
                    ������).*/

    private Map<Human, List<String>> phoneBook;

    public PhoneBook(){
        phoneBook = new HashMap<>();
    }

    public void add(Human human, String phone){

        if (human == null) throw new IllegalArgumentException("Human is null");
        if (phone == null) throw new IllegalArgumentException("Phone is null");

        if (!phoneBook.containsKey(human)){
            phoneBook.put(human, new ArrayList<>());
        }
        if(!phoneBook.get(human).contains(phone)) phoneBook.get(human).add(phone);
    }

    public void remove(Human human, String phone){

        if (human == null) throw new IllegalArgumentException("Human is null");
        if (phone == null) throw new IllegalArgumentException("Phone is null");
        if (phone.isEmpty()) throw new IllegalArgumentException("Phone is empty");

        if (phoneBook.containsKey(human)){ phoneBook.get(human).remove(phone);}

    }

    public List<String> getHumanPhones(Human human){

        if (human == null) throw new IllegalArgumentException("Human is null");

        return phoneBook.get(human);
    }

    public Human find(String phone){

        if (phone == null) throw new IllegalArgumentException("Phone is null");

        for (Map.Entry<Human,List<String>> entry: phoneBook.entrySet()) {

            if (entry.getValue().contains(phone)) return entry.getKey();
        }

        return null;
    }

    public PhoneBook findHumansAtChars(String name){

        if (name == null) throw new IllegalArgumentException("name is null");

        Map<Human, List<String>> result = new HashMap<>();

        for (Map.Entry<Human, List<String>> entry: phoneBook.entrySet()){
            if(entry.getKey() != null && entry.getKey().getFamilyName() != null
                    && entry.getKey().getFamilyName().startsWith(name)) result.put(entry.getKey(), entry.getValue());
        }

        PhoneBook n = new PhoneBook();
        n.phoneBook = result;

        return n;
    }
}
