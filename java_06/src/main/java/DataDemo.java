import java.util.ArrayList;
import java.util.List;

public class DataDemo {

    public static List<Integer> getAll(Data data){

        List<Integer> result = new ArrayList<>();

        for(Group gr: data){
            for (int i : gr.getArray()) {
                result.add(i);
            }
        }

        return result;
    }
}
