import java.util.Iterator;

public class Data implements Iterable<Group>{
/*    �������� ����� Data (����� �����). ����� ������ ��������� �������� ������ (������
    ��������) � ��� ����� � ���� �������. ������: ������������, ������� � �������
    �����, ����� �������. ������ ���� ����������� ������� ������ ���:
    Data data = new Data(�Test data�, new Group(�), new Group(�));
    ///////////////////////////////////////////////////////////////////////////////////
    �������� ��������, ������� ��� ������� ������ Data ���������� ��� ����� �� ���� ���
�������. �������� � ����� Data ����� iterator(). */

    private String name;
    private Group[] set;

    public Data(String name, Group...set){
        setName(name);
        setSet(set);
    }

    public String getName() {
        return name;
    }

    public void setName(String setName) {
        this.name = setName;
    }

    public Group[] getSet() {
        return set;
    }

    public void setSet(Group[] set) {
        this.set = set;
    }

    public int size(){return set.length;}

    @Override
    public Iterator<Group> iterator() {
        return new Iterator<Group>() {

            private int index;

            @Override
            public boolean hasNext() {
                return index < set.length;
            }

            @Override
            public Group next() {
                return set[index++];
            }
        };
    }
}
