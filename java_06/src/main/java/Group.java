public class Group {
/*    �������� ����� Group (������ ������). ����� ������ ��������� �������������
    ������ (����� �����) � ���� ������ (������ ����� �����). ������: ������������,
    ������� � ������� �����, ����� �������. ������ ���� ����������� ������� ������
    ���:
    Group group = new Group(100, 1, 2, 3); // 100 � ����� ������*/
    private int ID;
    private int[] array;

    public Group(){}
    public Group(int ID, int...array){
        setArray(array);
        setID(ID);
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int size(){return array.length;}
}
