import java.util.Objects;
/*Создайте класс Human с полями: фамилия, имя, отчество, возраст и методами:
        конструкторы, геттеры/сеттеры, equals и hashCode.
        Напишите метода класса ListDemo, который получает на вход список объектов типа Human
        и еще один объект типа Human. Результат — список однофамильцев заданного человека
        среди людей из входного списка.*/

public class Human implements Comparable<Human>{

    private String firstName;
    private String secondName;
    private String familyName;
    private int age;

    public Human(){
        this.firstName = "";
        this.secondName = "";
        this.familyName = "";
        this.age = 0;
    }

    public Human(String firstName, String secondName, String familyName, int age) {

        if (firstName == null || secondName == null || familyName == null || age < 0) throw new IllegalArgumentException("Param don't correct.");
        this.firstName = firstName;
        this.secondName = secondName;
        this.familyName = familyName;
        this.age = age;
    }

    public Human(Human human){
        this.firstName = new String(human.getFirstName());
        this.secondName = new String(human.getSecondName());
        this.familyName = new String(human.getFamilyName());
        this.age = human.getAge();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName != null) this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        if (secondName != null) this.secondName = secondName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        if (familyName != null) this.familyName = familyName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getAge() == human.getAge() && getFirstName().equals(human.getFirstName()) && getSecondName().equals(human.getSecondName()) && getFamilyName().equals(human.getFamilyName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getSecondName(), getFamilyName(), getAge());
    }

    @Override
    public int compareTo(Human other) {
        
        int res = firstName.compareTo(other.firstName);
        if (res != 0) return res;
        res = secondName.compareTo(other.secondName);
        if (res != 0) return res;
        return familyName.compareTo(other.familyName);

    }
    
}
