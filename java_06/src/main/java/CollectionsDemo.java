import java.util.*;

public class CollectionsDemo {

    public static int toCountStringBeginAtChar(List<String> list, char ch){
/*        Вход: список строк и символ. Выход: количество строк входного списка, у которых первый
        символ совпадает с заданным.*/
        int num = 0;
        for (String str: list) {
            if (str!=null && str.charAt(0) == ch) { num ++; }
        }
        return num;
    }

    public static List<Human> findNamesakes(List<Human> list, Human human){
/*        Напишите метода класса ListDemo, который получает на вход список объектов типа Human
        и еще один объект типа Human. Результат — список однофамильцев заданного человека
        среди людей из входного списка.*/
        if (human == null) throw new IllegalArgumentException("human is null");

        ArrayList<Human> resultList = new ArrayList<>();

        for (Human man: list) {
            if (man!=null && man.getFamilyName().equals(human.getFamilyName())){
                resultList.add(man);
            }
        }

        return resultList;
    }

    public static ArrayList<Human> copyListWithoutHuman(List<Human> list, Human human){
/*        Вход: список объектов типа Human и еще один объект типа Human. Выход — копия
        входного списка, не содержащая выделенного человека. При изменении элементов
        входного списка элементы выходного изменяться не должны.*/
        if (human == null) throw new IllegalArgumentException("Param is not correct");
        ArrayList<Human> resultList = new ArrayList<>();

        for (Human man: list) {
            if (man != null && !man.equals(human)){ resultList.add(new Human(man)); }
        }
        return resultList;
    }



    public static List<Set<Integer>> getNotIntersect(List<Set<Integer>> list, Set<Integer> set){
    /*Вход: список множеств целых чисел и еще одно множество. Выход: список всех множеств
входного списка, которые не пересекаются с заданным множеством.*/

        if (list == null || set == null) throw new IllegalArgumentException("Param is not correct");
        List<Set<Integer>> result = new ArrayList<>(list.size());
        for(Set<Integer> s: list){
            if (Collections.disjoint(s,set)){
                result.add(s);
            }
        }

        return result;
    }


    public static Set <? extends Human> findTheOldest(List<? extends Human> list){
        /*Напишите метод класса ListDemo, который получает на вход список, состоящий из
объектов типа Human и его производных классов. Результат — множество людей из
входного списка с максимальным возрастом.*/
        if (list == null) throw new IllegalArgumentException();

        Set<Human> set = new HashSet<>();

        if (list.get(0) == null) throw new IllegalArgumentException("");
        Human max = list.get(0);

        for (Human h: list){

            if (h.getAge() > max.getAge()){
                set.clear();
                set.add(h);
                max = h;
            }else if (h.getAge() == max.getAge()){
                set.add(h);
            }
        }

        return set;
    }

    public static List<Human> getSortList(Set<? extends Human> set) {
    /*По множеству объектов, расширяющих Human, постройте список так, чтобы итератор
     списка перебирал его элементы по возрастанию ФИО людей без дополнительной
     сортировки списка.*/
        TreeSet<Human> treeSet = new TreeSet<>(new HumanComparator());
        treeSet.addAll(set);
        List<Human> result = new ArrayList<>(set.size());
        result.addAll(treeSet);
        return result;
    }


    public static Set <Human> humansWithId(Map <Integer, Human> humansMap, Set <Integer> setID){
        /* 7.Имеется набор людей, каждому человеку задан уникальный целочисленный
идентификатор. Напишите метод, который получает на вход отображение (Map)
целочисленных идентификаторов в объекты типа Human и множество целых чисел.
Результат — множество людей, идентификаторы которых содержатся во входном
множестве*/

        if (humansMap == null) throw new IllegalArgumentException("Map is null");

        Set <Human> result = new HashSet<>(humansMap.size());

        if (setID != null){

            for (int i: setID){
                Human human = humansMap.get(i);
                if (human != null) result.add(human);
            }
        }

        return result;
    }

    public static Set <Human> humans18old(Map <Integer, Human> humansMap){
    /*Для отображения из задачи 7 постройте список идентификаторов людей, чей возраст не
менее 18 лет. */

        if (humansMap == null) throw new IllegalArgumentException("Map is null");

        Set <Human> result = new HashSet<>(humansMap.size());

        for(Map.Entry<Integer, Human> entry: humansMap.entrySet()){
            if (entry != null && entry.getValue().getAge() >= 18) result.add(entry.getValue());
        }

        return result;
    }

    public static Map<Integer, Integer> createMapIDandAges(Map<Integer, Human> humansMap){
        /*Для отображения из задачи 7 постройте новое отображение, которое идентификатору
        сопоставляет возраст человека.*/

        if (humansMap == null) throw new IllegalArgumentException("Map is null");
        Map<Integer, Integer> result = new HashMap<>();

        for (Map.Entry<Integer, Human> entry: humansMap.entrySet()){
            if (entry.getValue() != null) {
                result.put(entry.getKey(),entry.getValue().getAge());
            }
        }

        return result;

    }

    public static Map<Integer, List<Human>> getMapOfListsAndAgesID(Set<Human> set){
        /*По множеству объектов типа Human постройте отображение, которое целому числу
        (возраст человека) сопоставляет список всех людей данного возраста из входного
        множества.*/

        Map<Integer, List<Human>> result = new HashMap<>();
        int key;

        for(Human human: set){

            if (human != null) {

                key = human.getAge();

                if (!result.containsKey(key)){
                    result.put(key, new ArrayList<>());
                }
                result.get(key).add(human);
            }
        }

        return result;
    }

   public static Map<Integer, Map<Character, List<Human>>> getMapOfAgesAndListsOfCharAndHumanLists(Set<Human> set){
/*        С использованием решения задачи 10 по множеству людей постройте отображение,
 которое возрасту сопоставляет новое отображение, которое букве алфавита сопоставляет
 список всех людей из входного множества, имеющих данный возраст, и фамилия которых
 начинается на эту букву. Т.е.
 (возраст ->
 (буква -> <список людей данного возраста с фамилией на эту букву>)
 )
 Списки внутри должны быть отсортированы по ФИО по убыванию*/
        if (set == null) throw new IllegalArgumentException("set is null");

        Map<Integer, Map<Character, List<Human>>> result = new HashMap<>();

        TreeSet<Human> treeSet = new TreeSet<>(new HumanComparatorDescending());
        treeSet.addAll(set);
        Map<Integer, List<Human>> map = getMapOfListsAndAgesID(treeSet);//age->list

        for(Map.Entry<Integer, List<Human>> entry: map.entrySet()){

            result.put(entry.getKey(), new HashMap<>());
            char ch = 0;

            for(Human human: entry.getValue()){

                if (human == null) throw new IllegalArgumentException("set is null");
                if (ch != human.getFamilyName().charAt(0)) {
                    ch = human.getFamilyName().charAt(0);
                    result.get(entry.getKey()).put(ch, new ArrayList<>());
                }
                result.get(entry.getKey()).get(ch).add(human);
            }
        }

        return result;
    }
}
