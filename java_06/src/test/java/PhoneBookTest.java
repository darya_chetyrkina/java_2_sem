import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PhoneBookTest {

    @Test
    public void getPhones() {
        PhoneBook book = new PhoneBook();
        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 23);
        book.add(human1, "660-990");
        book.add(human1, "700-700");
        book.add(human2, "131-313");
        book.remove(human1, "660-990");
        List<String> phones = book.getHumanPhones(human1);
        List<String> expected = new ArrayList<>();
        expected.add("700-700");
        assertEquals(expected, phones);
    }

    @Test
    public void find() {

        PhoneBook book = new PhoneBook();
        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 23);
        book.add(human1, "660-990");
        book.add(human1, "700-700");
        book.add(human2, "131-313");
        Human human = book.find("131-313");
        assertEquals(human2,human);
    }

    @Test
    public void findHumans() {

        PhoneBook book = new PhoneBook();
        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 23);
        Human human3 = new Human("Ivan", "Ivanovich", "Ivonov", 25);
        book.add(human1, "660-990");
        book.add(human1, "700-700");
        book.add(human2, "131-313");
        book.add(human3, "88005553535");
        PhoneBook actual = book.findHumansAtChars("Iva");
        PhoneBook expected = new PhoneBook();
        expected.add(human1, "660-990");
        expected.add(human1, "700-700");
        expected.add(human2, "131-313");
        assertEquals(expected.getHumanPhones(human1),actual.getHumanPhones(human1));
        assertEquals(expected.getHumanPhones(human2),actual.getHumanPhones(human2));
        assertEquals(expected.getHumanPhones(human3),actual.getHumanPhones(human3));
    }
}