import static org.junit.Assert.*;

public class HumanTest {

    @org.junit.Test
    public void copy(){

        Human human = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human(human);
        human.setFamilyName("Alekseev");
        assertNotEquals(human, human2);
    }

    @org.junit.Test
    public void copy2(){

        Human human = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human(human);
        human2.setFamilyName("Alekseev");
        assertEquals("Ivanov", human.getFamilyName());
    }

    @org.junit.Test
    public void copy3(){

        Human human = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human(human);
        human2.setAge(30);
        assertEquals(20, human.getAge());
    }
}