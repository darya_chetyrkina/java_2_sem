import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class CollectionsDemoTest {

    @org.junit.Test
    public void toCountStringBeginAtChar() {
        ArrayList<String> list = new ArrayList<>(3);
        list.add("One");
        list.add("Two");
        list.add("Three");
        char ch = 'T';
        assertEquals(2,CollectionsDemo.toCountStringBeginAtChar(list, ch));

    }

    @org.junit.Test
    public void toCountStringBeginAtChar2() {
        ArrayList<String> list = new ArrayList<>(3);
        list.add("One");
        list.add("Two");
        list.add("Three");
        char ch = 'o';
        assertEquals(0,CollectionsDemo.toCountStringBeginAtChar(list, ch));

    }

    @Test
    public void findNamesakes() {
        ArrayList<Human> list1 = new ArrayList<>(4);
        Human h1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        list1.add(h1);
        Human h2 = new Human("Ilya", "Stepanovich", "Ivanov", 13);
        list1.add(h2);
        Human h3 = new Human("Darya", "Ivanovna", "Ivanova", 20);
        list1.add(h3);
        Human h4 = new Human("Ivan", "Ivanovich", "Stepaniv", 67);
        list1.add(h4);

        ArrayList<Human> expectedList = new ArrayList<>(2);
        expectedList.add(new Human(h1));
        expectedList.add(new Human(h2));

        assertEquals(expectedList, CollectionsDemo.findNamesakes(list1, list1.get(0)));

    }

/*
    @Test
    public void findNamesakesWith2() {
        ArrayList<Human> list1 = new ArrayList<>(4);
        Human h1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        list1.add(h1);
        Human h2 = new Human("Ilya", "Stepanovich", "Ivanov", 13);
        list1.add(h2);
        Human h3 = new Human("Darya", "Ivanovna", "Ivanova", 20);
        list1.add(h3);
        Human h4 = new Human("Ivan", "Ivanovich", "Stepaniv", 67);
        list1.add(h4);

        ArrayList<Human> expectedList = new ArrayList<>(2);
        expectedList.add(new Human(h1));
        expectedList.add(new Human(h2));

        ArrayList<Human> testList = CollectionsDemo.findNamesakes(list1, list1.get(0));
        h1.setFamilyName("Petrov");


        assertEquals(expectedList, testList);

    }
*/

    @Test
    public void copyWithoutHuman() {
        ArrayList<Human> list1 = new ArrayList<>(4);
        Human h1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        list1.add(h1);
        Human h2 = new Human("Ilya", "Stepanovich", "Ivanov", 13);
        list1.add(h2);
        Human h3 = new Human("Darya", "Ivanovna", "Ivanova", 20);
        list1.add(h3);
        Human h4 = new Human("Ivan", "Ivanovich", "Stepaniv", 67);
        list1.add(h4);

        ArrayList<Human> expectedList = new ArrayList<>(3);
        expectedList.add(new Human(h1));
        expectedList.add(new Human(h2));
        expectedList.add(new Human(h4));

        assertEquals(expectedList, CollectionsDemo.copyListWithoutHuman(list1,h3));

    }

    @Test
    public void copyWithoutHuman2() {
        ArrayList<Human> list1 = new ArrayList<>(4);
        Human h1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        list1.add(h1);
        Human h2 = new Human("Ilya", "Stepanovich", "Ivanov", 13);
        list1.add(h2);
        Human h3 = new Human("Darya", "Ivanovna", "Ivanova", 20);
        list1.add(h3);
        Human h4 = new Human("Ivan", "Ivanovich", "Stepaniv", 67);
        list1.add(h4);

        ArrayList<Human> expectedList = new ArrayList<>(3);
        expectedList.add(h1);
        expectedList.add(h2);
        expectedList.add(h4);

        ArrayList<Human> testList = CollectionsDemo.copyListWithoutHuman(list1,h3);
        h1.setFamilyName("Petrov");

        assertNotEquals(expectedList, testList);

    }

    @Test
    public void testGetNotIntersect(){

        Set<Integer> set1 = new HashSet<>(5);
        for(int i = 0; i < 5; i++){
            set1.add(i);
        }

        Set<Integer> set2 = new HashSet<>(4);
        for(int i = 5; i < 9; i++){
            set2.add(i);
        }

        Set<Integer> set3 = new HashSet<>(2);
        set3.add(9);
        set3.add(10);

        List<Set<Integer>> list = new ArrayList<>(3);
        list.add(set1);
        list.add(set2);
        list.add(set3);

        Set<Integer> set4 = new HashSet<>(2);
        set4.add(10);
        set4.add(9);

        List<Set<Integer>> expectedList = new ArrayList<>(2);
        expectedList.add(set1);
        expectedList.add(set2);

        assertEquals(expectedList, CollectionsDemo.getNotIntersect(list, set4));

    }

    @Test
    public void testGetNotIntersect2(){

        Set<Integer> set1 = new HashSet<>(5);
        for(int i = 0; i < 5; i++){
            set1.add(i);
        }

        Set<Integer> set2 = new HashSet<>(4);
        for(int i = 5; i < 9; i++){
            set2.add(i);
        }

        Set<Integer> set3 = new HashSet<>(2);
        set3.add(9);
        set3.add(10);

        List<Set<Integer>> list = new ArrayList<>(3);
        list.add(set1);
        list.add(set2);
        list.add(set3);

        Set<Integer> set4 = new HashSet<>(3);
        set4.add(10);
        set4.add(5);
        set4.add(1);

        List<Set<Integer>> expectedList = new ArrayList<>(0);

        assertEquals(expectedList, CollectionsDemo.getNotIntersect(list, set4));

    }

    @Test
    public void testOldest(){
        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 23);
        Human human3 = new Human("Ilya", "Ivanovich", "Ivanov", 25);
        Human student = new Student(new Human("Mitya", "Ivanovich", "Ivanov", 25), "эконом");
        List <Human> list = new ArrayList<>(4);
        list.add(human1);
        list.add(human2);
        list.add(human3);
        list.add(student);
        Set <Human> expected = new HashSet<>(2);
        expected.add(human3);
        expected.add(student);
        Set <? extends Human> set = CollectionsDemo.findTheOldest(list);
        assertEquals(expected, set);

    }

    @Test
    public void TestHumansWithID(){

        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 23);
        Human human3 = new Human("Ilya", "Ivanovich", "Ivanov", 25);

        Map<Integer, Human> humansMap = new HashMap<>(3);
        humansMap.put(1,human1);
        humansMap.put(2,human2);
        humansMap.put(3,human3);

        Set<Integer> setID = new HashSet<>(2);
        setID.add(3);
        setID.add(2);

        Set <Human> expectedList = new HashSet<>(2);
        expectedList.add(human3);
        expectedList.add(human2);

        assertEquals(expectedList, CollectionsDemo.humansWithId(humansMap,setID));


    }

    @Test
    public void TestHumansWithID2(){

        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 23);
        Human human3 = new Human("Ilya", "Ivanovich", "Ivanov", 25);

        Map<Integer, Human> humansMap = new HashMap<>(3);
        humansMap.put(1,human1);
        humansMap.put(2,human2);
        humansMap.put(3,human3);

        Set<Integer> setID = new HashSet<>(2);
        setID.add(4);
        setID.add(5);

        Set <Human> expectedList = new HashSet<>(0);

        assertEquals(expectedList, CollectionsDemo.humansWithId(humansMap,setID));

    }

    @Test(expected = IllegalArgumentException.class)
    public void TestHumansWithIDException(){
        CollectionsDemo.humansWithId(null,null);
    }

    @Test
    public void TestHumans18old(){

        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 20);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 10);
        Human human3 = new Human("Ilya", "Ivanovich", "Ivanov", 18);

        Map<Integer, Human> humansMap = new HashMap<>(3);
        humansMap.put(1,human1);
        humansMap.put(2,human2);
        humansMap.put(3,human3);

        Set <Human> expectedList = new HashSet<>(0);
        expectedList.add(human1);
        expectedList.add(human3);

        assertEquals(expectedList, CollectionsDemo.humans18old(humansMap));

    }

    @Test
    public void TestHumans18old2(){

        Human human1 = new Human("Ivan", "Ivanovich", "Ivanov", 1);
        Human human2 = new Human("Ivan", "Ivanovich", "Ivanov", 10);
        Human human3 = new Human("Ilya", "Ivanovich", "Ivanov", 17);

        Map<Integer, Human> humansMap = new HashMap<>(3);
        humansMap.put(1,human1);
        humansMap.put(2,human2);
        humansMap.put(3,human3);

        Set <Human> expectedList = new HashSet<>(0);

        assertEquals(expectedList, CollectionsDemo.humans18old(humansMap));

    }

    @Test(expected = IllegalArgumentException.class)
    public void TestHumans18old3Exception(){

        CollectionsDemo.humans18old(null);

    }

    @Test
    public void TestCreateMapIDandAges(){
        Human human1 = new Human("A","A","A",10);
        Human human2 = new Human("B","B","B",11);
        Human human3 = new Human("C","C","C",12);
        Human human4 = new Human("D","D","D",13);

        Map<Integer,Human> map = new HashMap<>(4);
        map.put(1, human1);
        map.put(2, human2);
        map.put(3, human3);
        map.put(4, human4);

        Map<Integer, Integer> expected = new HashMap<>(4);
        expected.put(1,10);
        expected.put(2,11);
        expected.put(3,12);
        expected.put(4,13);

        assertEquals(expected, CollectionsDemo.createMapIDandAges(map));
    }

    @Test
    public void TestGetMapOfListsAndAgesID(){
        Human human1 = new Human("A","A","A",11);
        Human human2 = new Human("B","B","B",11);
        Human human3 = new Human("C","C","C",12);
        Human human4 = new Human("D","D","D",13);

        Set<Human> set = new HashSet<>(4);
        set.add(human1);
        set.add(human2);
        set.add(human3);
        set.add(human4);

        Map<Integer, List<Human>> expected = new HashMap<>(4);
        expected.put(11,new ArrayList<>());
        expected.get(11).add(human1);
        expected.get(11).add(human2);
        expected.put(12,new ArrayList<>());
        expected.get(12).add(human3);
        expected.put(13,new ArrayList<>());
        expected.get(13).add(human4);

        assertEquals(expected, CollectionsDemo.getMapOfListsAndAgesID(set));
    }

    @Test
    public void getSortList(){
        Human human1 = new Human("Aa","A","A",11);
        Human human2 = new Human("Aa","B","B",11);
        Human human3 = new Human("AAa","C","C",12);
        Human human4 = new Human("D","D","D",13);
        Set<Human> set = new HashSet<>(4);
        set.add(human4);
        set.add(human1);
        set.add(human2);
        set.add(human3);
        List<Human> expected = new ArrayList<>();
        expected.add(human3);
        expected.add(human1);
        expected.add(human2);
        expected.add(human4);
        assertEquals(expected, CollectionsDemo.getSortList(set));
    }

    @Test
    public void getMapOfAgesAndListsOfCharAndHumanLists(){

        Human human1 = new Human("Aa","A","Aa",11);
        Human human2 = new Human("Ab","B","Ab",11);
        Human human3 = new Human("C","C","C",12);
        Human human4 = new Human("D","D","D",12);

        Set<Human> set = new HashSet<>(4);
        set.add(human2);
        set.add(human1);
        set.add(human3);
        set.add(human4);

        Map<Character, List<Human>> old12 = new HashMap<>(2);
        List<Human> list12old1 = new ArrayList<>(1);
        List<Human> list12old2 = new ArrayList<>(1);
        old12.put('D', list12old1);
        old12.put('C', list12old2);
        list12old1.add(human4);
        list12old2.add(human3);

        Map<Integer, Map<Character,List<Human>>> expected = new HashMap<>(3);
        Map<Character, List<Human>> old11 = new HashMap<>(1);
        List<Human> list11old = new ArrayList<>(2);
        list11old.add(human2);
        list11old.add(human1);
        old11.put('A', list11old);
        expected.put(11, old11);

        expected.put(12, old12);

        assertEquals(expected, CollectionsDemo.getMapOfAgesAndListsOfCharAndHumanLists(set));
    }

}
