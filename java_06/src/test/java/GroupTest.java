import org.junit.Test;

import static org.junit.Assert.*;

public class GroupTest {

    @Test
    public void constructor(){
        Group group = new Group(100, 1,2,3);
        assertArrayEquals(new int[]{1,2,3}, group.getArray());
    }

    @Test
    public void constructor2(){
        Group group = new Group(100, 1,2,3);
        assertEquals(new int[]{1,2,3}.length, group.getArray().length);
    }

}