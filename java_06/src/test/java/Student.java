public class Student extends Human{
    /*Создайте класс Student, производный от Human, новое поле — название факультета, к
нему геттер, сеттер и конструктор.
*/
    String faculty;

    Student(Human human, String faculty){
        super(human);
        if (faculty == null) throw new IllegalArgumentException("");
        setFaculty(faculty);
    }

    public String getFaculty(){return faculty;}
    public void setFaculty(String faculty){this.faculty = faculty;}


}
