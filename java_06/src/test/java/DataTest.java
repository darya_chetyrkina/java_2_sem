import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DataTest {

    @Test
    public void construstor(){
        Data data = new Data("Name", new Group(100,1,2,3),new Group(200,4,5,6),
                new Group(300,7,8,9));
        Group[] exp = new Group[3];
        exp[0] = new Group(100,1,2,3);
        exp[1] = new Group(200,4,5,6);
        exp[2] = new Group(300,7,8,9);
        int i = 0;
        for (Group gr: data) {
            assertArrayEquals(exp[i].getArray(), gr.getArray());
            i++;
        }
    }

    @Test
    public void DataDemo(){
        Data data = new Data("Name", new Group(100,1,2,3),new Group(200,4,5,6),
                new Group(300,7,8,9));
        List<Integer> list = DataDemo.getAll(data);
        List<Integer> exp = new ArrayList<>(9);
        for (int i = 1; i < 10;i++){
            exp.add(i);
        }

        assertEquals(exp,list);
    }

}