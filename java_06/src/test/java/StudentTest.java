import org.junit.Test;

public class StudentTest {

    @Test (expected = IllegalArgumentException.class)
    public void testConstruct(){
        Human human = new Human();
        Student student= new Student(human, null);
    }

}